const int fanControlPin = 1;
const int potSensorPin = 3;

void setup() {
  pinMode(fanControlPin, OUTPUT);
  pinMode(potSensorPin, INPUT);
}

void loop() {
  int potValue = analogRead(potSensorPin);
  int fanSpeed = map(potValue, 0, 1023, 0, 255);

  analogWrite(fanControlPin, 255 - fanSpeed);
  delay(10);
}

# attiny85_pwm_fan_controller

PWM speed control for 12 volt brushless DC fans using a potentiometer.

![Schematic](attiny85_pwm_fan_controller_schematic.png)